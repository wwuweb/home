; Core version
; ------------

core = 7.x

; API version
; ------------

api = 2

; Core project
; -----------

projects[drupal][type] = core

; Collegesites profile
; --------------------

projects[collegesites][download][type] = "git"
projects[collegesites][download][tag] = "7.x-7.1.1"
projects[collegesites][download][url] = "https://bitbucket.org/wwuweb/collegesites.git"
projects[collegesites][type] = "module"

; Base Modules
; ------------

projects[colors][version] = "1.0-rc1"

projects[picture][version] = "2.11"

projects[fullcalendar][version] = "2.0"

projects[media][version] = "2.0-beta1"

projects[menu_expanded][version] = "1.0-beta1"

projects[node_export][version] = "3.1"

projects[references][version] = "2.1"

projects[paragraphs][version] = "1.0-rc5"

projects[ultimate_cron][version] = "2.0"

projects[uuid][version] = "1.0-alpha6"

projects[viewreference][version] = "3.5"

projects[views_datasource][version] = "1.0-alpha2"

projects[views_slideshow][version] = "3.1"

projects[wysiwyg][version] = "2.3"


; Ancilliary Modules
; ------------------

projects[ajaxblocks][version] = "1.4"

projects[breakpoints][version] = "1.4"

projects[field_display_label][version] = "1.3"

projects[focal_point][version] = "1.0"


; Custom Themes
; -------------

projects[wwuzen-home][download][type] = "git"
projects[wwuzen-home][download][url] = "https://bitbucket.org/wwuweb/wwuzen-home"
projects[wwuzen-home][type] = "theme"

; Libraries
; ---------

libraries[fullcalendar][download][type] = "file"
libraries[fullcalendar][download][url] = "https://github.com/fullcalendar/fullcalendar/releases/download/v1.6.0/fullcalendar-1.6.0.zip"
libraries[fullcalendar][download][subtree] = "fullcalendar-1.6.0/fullcalendar"
libraries[fullcalendar][type] = "library"

libraries[jquery.cycle][download][type] = "file"
libraries[jquery.cycle][download][url] = "http://malsup.github.io/min/jquery.cycle.all.min.js"
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][type] = "library"
